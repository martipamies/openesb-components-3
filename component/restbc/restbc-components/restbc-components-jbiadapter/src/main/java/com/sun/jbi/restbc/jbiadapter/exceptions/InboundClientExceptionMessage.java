/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sun.jbi.restbc.jbiadapter.exceptions;
import java.io.Serializable;
import java.util.List;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author polperez
 */

@XmlRootElement
public class InboundClientExceptionMessage implements Serializable {

    private int         mFaultCode;
    private Status      mFaultStatus;
    private String      mPath; 
    private String      mMethod; 
    private MediaType   mConsumedType ; 
    private List<MediaType> mProduceType;
    private String      mFaultMessage;
    private String      mTechnicalFaultMessage;

    /**
     * default constructor
     */
    public InboundClientExceptionMessage () {
        
    }
    
    
    /**
     * @return the mPath
     */
    public String getPath() {
        return mPath;
    }

    /**
     * @param mPath the mPath to set
     */
    public void setPath(String path) {
        this.mPath = path;
    }

    /**
     * @return the mMethod
     */
    public String getMethod() {
        return mMethod;
    }

    /**
     * @param method
     */
    public void setMethod(String method) {
        this.mMethod = method;
    }

    /**
     * @return the mConsumedType
     */
    public MediaType getConsumedType() {
        return mConsumedType;
    }

    /**
     * @param consumedType the mConsumedType to set
     */
    public void setConsumedType(MediaType consumedType) {
        this.mConsumedType = consumedType;
    }

    /**
     * @return the mProduceType
     */
    public List<MediaType> getProduceType() {
        return mProduceType;
    }

    /**
     * @param produceType the mProduceType to set
     */
    public void setProduceType(List<MediaType> produceType) {
        this.mProduceType = produceType;
    }

    /**
     * @return the mFaultStatus
     */
    public Status getFaultStatus() {
        return mFaultStatus;
    }

    /**
     * @param mFaultStatus the mFaultStatus to set
     */
    public void setFaultStatus(Status mFaultStatus) {
        this.mFaultStatus = mFaultStatus;
    }

    /**
     * @return the mFaultMessage
     */
    public String getFaultMessage() {
        return mFaultMessage;
    }

    /**
     * @param mFaultMessage the mFaultMessage to set
     */
    public void setFaultMessage(String mFaultMessage) {
        this.mFaultMessage = mFaultMessage;
    }
    
    /**
     * @return the mTechnicalFAultMessage
     */
    public String getTechnicalFaultMessage() {
        return mTechnicalFaultMessage;
    }

    /**
     * @param technicalFaultMessage
     */
    public void setTechnicalFaultMessage(String technicalFaultMessage) {
        this.mTechnicalFaultMessage = technicalFaultMessage;
    }
    
    /**
     * @return the mFaultCode
     */
    public int getFaultCode() {
        return mFaultCode;
    }

    /**
     * @param mFaultCode the mFaultCode to set
     */
    public void setFaultCode(int mFaultCode) {
        this.mFaultCode = mFaultCode;
    }
    
    @Override
     public String toString () {
         StringBuilder sb = new StringBuilder() ; 
         sb.append("Fault Inbound Message: ");
         
         if (!(this.getFaultStatus().getStatusCode() == 0)) {
             sb.append("Fault Code: ").append(getFaultStatus().getStatusCode()).append("; ");
             sb.append("Fault Status: ").append(getFaultStatus()).append("; ");
         }
         
         if (!(getPath() == null)) {
             sb.append("Path: ").append(getPath()).append("; ");
         }         
         if (!(getMethod() == null)) {
             sb.append("Method: ").append(getMethod()).append("; "); 
         }         
         if (!(getFaultMessage() == null)) { 
             sb.append("Fault Message: ").append(getFaultMessage()).append("; ");
         }
         if (!(getTechnicalFaultMessage() == null)) { 
             sb.append("Technical Fault Message: ").append(getTechnicalFaultMessage());
         }
         
        return sb.toString() ;
    }

}
