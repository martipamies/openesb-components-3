package com.sun.jbi.engine.bpel.core.bpel.model.runtime.impl;

import com.sun.jbi.engine.bpel.core.bpel.engine.BPELProcessInstance;
import com.sun.jbi.engine.bpel.core.bpel.engine.Engine;

import java.util.Iterator;
import java.util.ServiceLoader;
import java.util.logging.Logger;

/**
 * @author theyoz
 * @since 26/09/18
 */
public class MonitorManagerFactory {
    private static final Logger LOGGER = Logger.getLogger(MonitorManagerFactory.class.getName());
    private static MonitorManagerSpi factory;

    public static MonitorManager newInstance(Engine engine, BPELProcessInstance instance) {
        if (factory == null) {
            ServiceLoader<MonitorManagerSpi> serviceLoader = ServiceLoader.load(MonitorManagerSpi.class);

            Iterator<MonitorManagerSpi> ite = serviceLoader.iterator();

            if (ite.hasNext()) {
                factory = ite.next();

                if (ite.hasNext()) {
                    LOGGER.warning(
                            String.format("Multiple MonitorManagerSpi found, using %s", factory.getClass().getName()));
                }
            } else {
                factory = new MonitorManagerSpi();
            }
        }

        return factory.create(engine, instance);
    }
}
