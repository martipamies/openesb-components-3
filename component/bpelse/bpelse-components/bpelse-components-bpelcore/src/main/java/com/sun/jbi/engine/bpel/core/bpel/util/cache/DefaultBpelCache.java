package com.sun.jbi.engine.bpel.core.bpel.util.cache;

import com.sun.jbi.engine.bpel.core.bpel.util.BpelCache;
import java.util.HashMap;
import java.util.Map;
import org.w3c.dom.Node;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 */
public class DefaultBpelCache implements BpelCache {
    
    private Map<String, Node> cache = new HashMap<String, Node>();

    public String put(String key, Node value) {
        cache.put(key, value);
        return key;
    }

    public Node get(String key) {
        return cache.get(key);
    }

    public Node remove(String key) {
        return cache.remove(key);
    }
}
