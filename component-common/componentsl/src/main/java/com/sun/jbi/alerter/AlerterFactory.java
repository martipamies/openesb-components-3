package com.sun.jbi.alerter;

import java.util.Iterator;
import java.util.ServiceLoader;
import java.util.logging.Logger;

/**
 * @author theyoz
 * @since 27/09/18
 */
public class AlerterFactory {
    private AlerterFactory() {}

    private static final Logger LOGGER = Logger.getLogger(AlerterFactory.class.getName());
    private static AlerterSpi factory;
    private static Alerter defaultInstance;

    public static AlerterSpi getFactory() {
        if (factory == null) {
            ServiceLoader<AlerterSpi> serviceLoader = ServiceLoader.load(AlerterSpi.class);

            Iterator<AlerterSpi> ite = serviceLoader.iterator();

            if (ite.hasNext()) {
                factory = ite.next();

                if (ite.hasNext()) {
                    LOGGER.warning(
                            String.format("Multiple AlerterFactory found, using %s", factory.getClass().getName()));
                }
            } else {
                factory = new AlerterSpi();
            }
        }

        return factory;
    }

    public static Alerter newInstance() {
        return getFactory().create();
    }

    public static Alerter getDefaultInstance() {
        if (defaultInstance == null) {
            defaultInstance = getFactory().create();
        }

        return defaultInstance;
    }
}
