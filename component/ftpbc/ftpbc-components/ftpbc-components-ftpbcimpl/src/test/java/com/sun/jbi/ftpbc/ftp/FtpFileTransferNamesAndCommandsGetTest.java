/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FtpFileTransferNamesAndCommandsGetTest.java 
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */

package com.sun.jbi.ftpbc.ftp;

import org.testng.annotations.Test;

/**
 * JUnit based test.
 *
 * 
 * 
 * 
 * @author Harry Liu (harry.liu@sun.com)
 * @author theyoz
 *
 * Copyright 2006 Sun Microsystems, Inc. All Rights Reserved.
 */
public class FtpFileTransferNamesAndCommandsGetTest extends AbstractFtpTest {
    FtpFileTransferNamesAndCommandsGet instance;

    @Override
    protected void afterInitializeFtp() throws Exception {
        super.afterInitializeFtp();

        instance = new FtpFileTransferNamesAndCommandsGet(ftp);
    }


    /**
     * Test of resolveTargetLocation method, of class com.sun.jbi.ftpbc.ftp.FtpFileTransferNamesAndCommandsGet.
     */
    @Test
    public void testResolveTargetLocation() throws Exception {
        System.out.println("resolveTargetLocation");

        instance.resolveTargetLocation();
    }

    /**
     * Test of constructor, of class com.sun.jbi.ftpbc.ftp.FtpFileTransferNamesAndCommandsGet.
     */
    @Test
    public void testFtpFileTransferNamesAndCommandsGet() throws Exception {
        System.out.println("FtpFileTransferNamesAndCommandsGet");
        
        new FtpFileTransferNamesAndCommandsGet(ftp);
        new FtpFileTransferNamesAndCommandsGet(new FtpFileTransferNamesAndCommandsGet(ftp), new FtpFileConfiguration(ftp));
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testFtpFileTransferNamesAndCommandsGetFails1() {
        new FtpFileTransferNamesAndCommandsGet(null);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testFtpFileTransferNamesAndCommandsGetFails2() {
        new FtpFileTransferNamesAndCommandsGet(null, null);
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testFtpFileTransferNamesAndCommandsGetFails3() throws Exception {
        new FtpFileTransferNamesAndCommandsGet(null, new FtpFileConfiguration(ftp));
    }

    @Test(expectedExceptions = NullPointerException.class)
    public void testFtpFileTransferNamesAndCommandsGetFails4()  {
        new FtpFileTransferNamesAndCommandsGet(new FtpFileTransferNamesAndCommandsGet(ftp), null);
    }
}
