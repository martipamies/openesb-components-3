package com.sun.jbi.engine.bpel.core.bpel.cache;

import javax.naming.InitialContext;
import org.w3c.dom.Node;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author Paul Perez 
 * Change Cache Method JIRA ESBCOMP-159
 * Date: 10/7/18
 */
public interface CacheManager {
    
    /**
     * This method is used to init the cache 
     * @throws CacheManagerException 
     */
    public void init (InitialContext context) throws CacheManagerException ;
    
    /**
     * Used when the BPEL is shutdown
     * @throws CacheManagerException 
     */
    public void shutdown () throws CacheManagerException ; 
    
    /**
     * @param key
     * @return boolean 
     * Check if a key exists in the cache 
     */
    boolean contains (String key);
    
    /**
     * @param key
     * @return Node 
     * Return the node stored in the cache for the Key
     */
    Node get(String key);
    
    /**
     * @param key
     * @param value
     * @return String
     * Return the Key used to Store the value. The returned key can be different from the input Key. Ex: For security reasons
     */
    String putGetKey (String key, Node value);
    
    /**
     * @param key
     * @param value
     * @return A node 
     * Return the Node entered as parameter
     */
    Node putGetValue (String key, Node value);
    
    /**
     * 
     * @param key
     * @param value
     * @return a Node 
     * Behave like a Java Map and return the existing value for the Key in the cache else return null
     */
    Node putGetOldValue (String key, Node value);
    
    /**
     * 
     * @param key
     * @return Node 
     * Remove the value associated with the Key and return it
     */
    Node remove(String key);
    
    /**
     * @return the classLoader Name String 
     */
    String getCacheManagerName ();
}
