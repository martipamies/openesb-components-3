package com.sun.jbi.restbc.jbiadapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.core.MediaType;
import javax.wsdl.Definition;

import com.sun.jbi.restbc.jbiadapter.descriptor.EndpointIdentifier;
import com.sun.jbi.restbc.jbiadapter.mbeans.RuntimeConfig;
import com.sun.jbi.restbc.jbiadapter.util.JsonUtil;
import com.sun.jbi.restbc.jbiadapter.util.PropertiesUtil;
import com.sun.jbi.restbc.jbiadapter.wsdl.RestOperation;

/**
 * OutboundConfiguration.java
 *
 * @author Edward Chou
 * @author Paul Perez ESB-161 Add JSON and Text/Plain Support
 *
 */
public class OutboundConfiguration {

    /**
     * @return the mRootRemoved
     */
    public boolean isRootRemoved() {
        return mRootRemoved;
    }

    /**
     * @param mRootRemoved the mRootRemoved to set
     */
    public void setRootRemoved(boolean mRootRemoved) {
        this.mRootRemoved = mRootRemoved;
    }

    private final static String URL_PROP = "url";
    private final static String METHOD_PROP = "method";
    private final static String CONTENT_TYPE_PROP = "content-type";
    private final static String ACCEPT_TYPES_PROP = "accept-types";
    private final static String ACCEPT_LANGUAGES_PROP = "accept-languages";
    private final static String HEADERS_PROP = "headers";
    private final static String PARAM_STYLE_PROP = "param-style";
    private final static String PARAMS_PROP = "params";
    private final static String FORWARD_AS_ATTACHMENT_PROP = "forward-as-attachment";
    private final static String BASICAUTH_USERNAME_PROP = "basic-auth-username";
    private final static String BASICAUTH_PASSWORD_PROP = "basic-auth-password";
    private final static String MSG_TYPE = "message-type";
    private final static String RESPONSE_ROOT_REQUIRED = "response-root-required";
    private final static String RESPONSE_ROOT_NAME = "response-root-name";
    private final static String REQUEST_ROOT_REMOVED = "request-root-removed";

    private String url;
    private String method;
    private String contentType;
    private List<String> acceptTypes = new ArrayList<String>();
    private List<MediaType> acceptMediaTypes = new ArrayList<MediaType>();
    private List<String> acceptLanguages = new ArrayList<String>();
    private Map<String, String> headers = new HashMap<String, String>();
    private String paramStyle;
    private Map<String, String> params = new HashMap<String, String>();
    private boolean forwardAsAttachment = false;
    private String basicAuthUsername;
    private String basicAuthPassword;
    private String msgType;

    private ServiceUnit serviceUnit;
    private RestOperation restOp;
    private Definition definition;
    private EndpointIdentifier endpointIdentifier;

    /**
     * Outbound properties add properties root required and root name in the case of
     * the JSON answer has not a root element.
     */
    private boolean mRootRequired;
    private String mRootName;
    
    /**
     * Outbound properties add properties remove root in the case of
     * the JSON request root must be removed
     */
    private boolean mRootRemoved ;

    /**
     * @param outputProperties
     * @param serviceUnit
     * @param restOp
     * @param definition
     * @param endpointIdentifier
     * @param runtimeConfig
     * @throws Exception
     */
    public OutboundConfiguration(Properties outputProperties,
            ServiceUnit serviceUnit,
            RestOperation restOp,
            Definition definition,
            EndpointIdentifier endpointIdentifier,
            RuntimeConfig runtimeConfig) throws Exception {
        this.serviceUnit = serviceUnit;
        this.restOp = restOp;
        this.definition = definition;
        this.endpointIdentifier = endpointIdentifier;

        String appConfigName = endpointIdentifier.getApplicationConfigurationName();
        if (appConfigName != null && appConfigName.length() > 0) {
            Map appConfigMap = runtimeConfig.retrieveApplicationConfigurationsMap();
            url = (String) appConfigMap.get(appConfigName);
            if (url == null) {
                throw new Exception("cannot resolve Application Configuration named: " + appConfigName);
            }
        } else {
            url = PropertiesUtil.safeGetProperty(outputProperties, URL_PROP);
        }
        method = PropertiesUtil.safeGetProperty(outputProperties, METHOD_PROP, "GET");
        contentType = PropertiesUtil.safeGetProperty(outputProperties, CONTENT_TYPE_PROP);
        acceptTypes = JsonUtil.parseJsonList(PropertiesUtil.safeGetProperty(outputProperties, ACCEPT_TYPES_PROP));
        for (String s : acceptTypes) {
            acceptMediaTypes.add(MediaType.valueOf(s));
        }
        acceptLanguages = JsonUtil.parseJsonList(PropertiesUtil.safeGetProperty(outputProperties, ACCEPT_LANGUAGES_PROP));
        headers = JsonUtil.parseJsonPairs(PropertiesUtil.safeGetProperty(outputProperties, HEADERS_PROP));
        paramStyle = PropertiesUtil.safeGetProperty(outputProperties, PARAM_STYLE_PROP, "QUERY");
        params = JsonUtil.parseJsonPairs(PropertiesUtil.safeGetProperty(outputProperties, PARAMS_PROP));
        forwardAsAttachment = Boolean.parseBoolean(PropertiesUtil.safeGetProperty(outputProperties, FORWARD_AS_ATTACHMENT_PROP, "false"));

        // basic authentication
        basicAuthUsername = PropertiesUtil.safeGetProperty(outputProperties, BASICAUTH_USERNAME_PROP);
        basicAuthUsername = PropertiesUtil.applyApplicationVariables(basicAuthUsername, runtimeConfig.retrieveApplicationVariablesMap());
        basicAuthPassword = PropertiesUtil.safeGetProperty(outputProperties, BASICAUTH_PASSWORD_PROP);
        basicAuthPassword = PropertiesUtil.applyApplicationVariables(basicAuthPassword, runtimeConfig.retrieveApplicationVariablesMap());

        msgType = PropertiesUtil.safeGetProperty(outputProperties, MSG_TYPE);

        // What to do with the json response. Do we add an XML root 
        if ("true".equals(PropertiesUtil.safeGetProperty(outputProperties, RESPONSE_ROOT_REQUIRED, "false"))) {
            this.setRootRequired(true);
            String rootName = PropertiesUtil.safeGetProperty(outputProperties, RESPONSE_ROOT_NAME,"rootRest").trim();
            if ((rootName != null) && (rootName.length() > 0)) {
                this.setRootName(rootName);
            }
        }
        
        // What to do with the json request. Do we remove the add an XML root 
        if ("true".equals(PropertiesUtil.safeGetProperty(outputProperties, REQUEST_ROOT_REMOVED, "false"))) {
            this.setRootRemoved(true);
        }        
    }

    /**
     * @return the serviceUnit
     */
    public ServiceUnit getServiceUnit() {
        return serviceUnit;
    }

    /**
     * @return the restOp
     */
    public RestOperation getRestOp() {
        return restOp;
    }

    /**
     * @return the definition
     */
    public Definition getDefinition() {
        return definition;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @return the method
     */
    public String getMethod() {
        return method;
    }

    /**
     * @return the contentType
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * @return the acceptMediaTypes
     */
    public List<MediaType> getAcceptMediaTypes() {
        return Collections.unmodifiableList(acceptMediaTypes);
    }

    /**
     * @return the acceptLanguages
     */
    public List<String> getAcceptLanguages() {
        return Collections.unmodifiableList(acceptLanguages);
    }

    /**
     * @return the headers
     */
    public Map<String, String> getHeaders() {
        return Collections.unmodifiableMap(headers);
    }

    /**
     * @return the paramStyle
     */
    public String getParamStyle() {
        return paramStyle;
    }

    /**
     * @return the params
     */
    public Map<String, String> getParams() {
        return Collections.unmodifiableMap(params);
    }

    /**
     * @return the forwardAsAttachment
     */
    public boolean isForwardAsAttachment() {
        return forwardAsAttachment;
    }

    /**
     * @return the basicAuthUsername
     */
    public String getBasicAuthUsername() {
        return basicAuthUsername;
    }

    /**
     * @return the basicAuthPassword
     */
    public String getBasicAuthPassword() {
        return basicAuthPassword;
    }

    /**
     * @return the msgType
     */
    public String getMsgType() {
        return msgType;
    }

    /**
     * @return the endpointIdentifier
     */
    public EndpointIdentifier getEndpointIdentifier() {
        return endpointIdentifier;
    }

    /**
     * @return the mRootRequired
     */
    public boolean isRootRequired() {
        return mRootRequired;
    }

    /**
     * @param mRootRequired the mRootRequired to set
     */
    public void setRootRequired(boolean mRootRequired) {
        this.mRootRequired = mRootRequired;
    }

    /**
     * @return the mRootName
     */
    public String getRootName() {
        return mRootName;
    }

    /**
     * @param mRootName the mRootName to set
     */
    public void setRootName(String mRootName) {
        this.mRootName = mRootName;
    }

}
