/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sun.jbi.engine.bpel.core.bpel.trace;

import com.sun.jbi.engine.bpel.core.bpel.util.I18n;
import java.util.Iterator;
import java.util.ServiceLoader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * BPELTraceManagerFactory is designed to provide many BPEL trace manager 
 * regarding the configuration of the services defined.
 * The class offers a lazy initialisation through the init method. 
 * 
 * @author polperez
 */
public class BPELTraceManagerFactory {

    private static BPELTraceManager traceManager;
    private static boolean initializationDone = false;

    private static final Logger LOGGER = Logger.getLogger(BPELTraceManagerFactory.class.getName());

    public BPELTraceManagerFactory() {
        LOGGER.log(Level.FINE, I18n.loc("BPELTraceManagerFactory constructor in process"));
    }

    public static void init() {
        LOGGER.log(Level.FINE, I18n.loc("Init method: load the BPELTrace manager"));
        ServiceLoader<BPELTraceManager> serviceLoader = ServiceLoader.load(BPELTraceManager.class);
        Iterator<BPELTraceManager> it = serviceLoader.iterator();
        if (it.hasNext()) {
            traceManager = it.next();
            LOGGER.log(Level.FINE, I18n.loc("Init method: The BPEL trace manager selected is : " + traceManager.getClass().getName()));
            if (it.hasNext()) {
                LOGGER.log(Level.WARNING, I18n.loc("Init method: Warning, there are more than one BPEL trace manager in the classpath"));
            }             
        } else {
            traceManager = new BPELTraceManagerImpl ();
        }
        initializationDone = true;
    }

    /**
     * @return the traceManager
     */
    public static BPELTraceManager getTraceManager() {
        if (!initializationDone) {
            BPELTraceManagerFactory.init();
        }
        return traceManager;
    }
}
