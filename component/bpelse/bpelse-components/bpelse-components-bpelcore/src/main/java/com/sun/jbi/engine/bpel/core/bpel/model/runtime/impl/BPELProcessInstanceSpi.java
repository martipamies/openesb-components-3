package com.sun.jbi.engine.bpel.core.bpel.model.runtime.impl;

import com.sun.jbi.engine.bpel.core.bpel.engine.BPELProcessInstance;
import com.sun.jbi.engine.bpel.core.bpel.engine.BPELProcessManager;
import com.sun.jbi.engine.bpel.core.bpel.engine.Engine;

/**
 * @author theyoz
 * @since 26/09/18
 */
public class BPELProcessInstanceSpi {
    public BPELProcessInstance create(BPELProcessManager processManager, Engine eng, String bpInstanceId) {
        return new BPELProcessInstanceImpl(processManager, eng, bpInstanceId);
    }
}
