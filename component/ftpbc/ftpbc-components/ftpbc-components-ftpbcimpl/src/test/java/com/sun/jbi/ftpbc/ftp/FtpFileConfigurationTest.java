package com.sun.jbi.ftpbc.ftp;

import com.sun.jbi.ftpbc.Endpoint;
import com.sun.jbi.ftpbc.EndpointImpl;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.text.WordUtils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import javax.xml.namespace.QName;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.security.SecureRandom;
import java.util.*;

/**
 * TestNG based test.
 *
 * @author theyoz
 */
public class FtpFileConfigurationTest extends AbstractFtpTest {
    private static Random rnd = new SecureRandom();

    private static Object[] getDefault(Class<?> clazz) {
        if (clazz.equals(String.class)) {
            return new Object[] {UUID.randomUUID().toString()};
        } else if (clazz.equals(boolean.class) || clazz.equals(Boolean.class)) {
            return new Object[] {true, false};
        } else if (clazz.equals(int.class) || clazz.equals(Integer.class)) {
            return new Object[] {Math.abs(rnd.nextInt())};
        } else if (clazz.equals(Endpoint.class) ) {
            return new Object[] {new EndpointImpl()};
        } else if (clazz.equals(QName.class) ) {
            return new Object[] {new QName(UUID.randomUUID().toString())};
        } else if (clazz.equals(long.class) || clazz.equals(Long.class)) {
            return new Object[] {Math.abs(rnd.nextLong())};
        } else {
            throw new UnsupportedOperationException(
                    String.format("Property of type %s unsupported", clazz.getName()));
        }
    }

    private Object[] getGoodPermutations(String property, Class<?> clazz) {
        try {
            Method method = this.getClass().getMethod("data" + WordUtils.capitalize(property));

            if (method != null && method.getReturnType().equals(Object[].class)) {
                return (Object[])method.invoke(this);
            } else {
                return getDefault(clazz);
            }
        } catch (Throwable t) {
            if (!(t instanceof NoSuchMethodException)) {
                t.printStackTrace();
            }

            return getDefault(clazz);
        }
    }

    FtpFileConfiguration instance;

    @Override
    protected void beforeInitializeFtp(Properties props) {
        super.beforeInitializeFtp(props);

        props.put("General Settings/Connection Mode", "Manual");
    }

    @Override
    protected void afterInitializeFtp() throws Exception {
        super.afterInitializeFtp();
        instance = ftp.getConfiguration();
    }

    @DataProvider
    public Object[][] dpGettersAndSetters() {
        return new Object[][]{
                {"append"},
                {"clientClassName"},
                {"commandConnectionTimeout"},
                {"controlChannelEncoding"},
                {"currentEndpoint"},
                {"currentOperationName"},
                {"dataConnectionTimeout"},
                {"directoryListingStyle"},
                {"enableCCC"},
                {"encryptedPassword"},
                {"hostName"},
                {"keyAlias"},
                {"keyPassword"},
                {"keyStoreLoc"},
                {"keyStorePassword"},
                {"maxRetry"},
                {"maxSequenceNumber"},
                {"mode"},
                {"postDirectoryName"},
                {"postDirectoryNameIsPattern"},
                {"postFileName"},
                {"postFileNameIsPattern"},
                {"postTransferCommand"},
                {"postTransferRawCommands"},
                {"preDirectoryName"},
                {"preDirectoryNameIsPattern"},
                {"preFileName"},
                {"preFileNameIsPattern"},
                {"preTransferCommand"},
                {"preTransferRawCommands"},
                {"providerClassName"},
                {"retryInterval"},
                {"secureFTPType"},
                {"sequenceNumberPersistenceMedia"},
                {"serverPort"},
                {"socksEnabled"},
                {"socksEncryptedPassword"},
                {"socksHostName"},
                {"socksServerPort"},
                {"socksUserName"},
                {"socksVersion"},
                {"stageDirectoryName"},
                {"stageEnabled"},
                {"stageFileName"},
                {"startingSequenceNumber"},
                {"targetDirectoryName"},
                {"targetDirectoryNameIsPattern"},
                {"targetFileName"},
                {"targetFileNameIsPattern"},
                {"trustStoreLoc"},
                {"trustStorePassword"},
                {"usePASV"},
                {"userHeuristicsLocation"},
                {"userName"}
        };
    }

    @Test(dataProvider = "dpGettersAndSetters")
    public void testGetterAndSetter(String property) throws Exception {
        PropertyDescriptor pd = PropertyUtils.getPropertyDescriptor(instance, property);

        Object saved = PropertyUtils.getProperty(instance, property);

        try {
            Object[] perms = getGoodPermutations(property, pd.getPropertyType());

            for (Object o : perms) {
                BeanUtils.setProperty(instance, property, o);

                Object value = PropertyUtils.getProperty(instance, property);

                assert Objects.equals(o, value) :
                        String.format(
                                "Property %s was expected to be [%s] but was [%s] instead.", property, o, value);
            }
        } catch (Exception e) {
            e.printStackTrace();

            throw e;
        } finally {
            PropertyUtils.setProperty(instance, property, saved);
        }
    }

    public Object[] dataMaxSequenceNumber() {
        return new Object[] {1L, 100L, 1000L};
    }

    public Object[] dataClientClassName() {
        return new Object[] {FtpFileClientImpl.class.getName()};
    }

    public Object[] dataControlChannelEncoding() {
        return new Object[] {"UTF-8"};
    }

    public Object[] dataDirectoryListingStyle() {
        return new Object[] {"UNIX", "NT 4.0"};
    }

    public Object[] dataEncryptedPassword() throws Exception {
        return new Object[] {NonEmptyScEncrypt.encrypt("123456789", "APASSWORD")};
    }

    public Object[] dataMode() throws Exception {
        return new Object[] {"Ascii", "Binary", "Ebcdic"};
    }

    public Object[] dataPostTransferCommand() throws Exception {
        return new Object[] {"None", "Delete", "Rename"};
    }

    public Object[] dataPreTransferCommand() throws Exception {
        return new Object[] {"Copy", "Rename", "None"};
    }

    public Object[] dataProviderClassName() throws Exception {
        return new Object[] {FtpFileProviderImpl.class.getName()};
    }

    public Object[] dataSequenceNumberPersistenceMedia() throws Exception {
        return new Object[] {"DB", "FlatFile"};
    }

    public Object[] dataServerPort() throws Exception {
        return new Object[] {1024};
    }

    public Object[] dataSocksEncryptedPassword() throws Exception {
        return new Object[] {NonEmptyScEncrypt.encrypt("123456789", "APASSWORD")};
    }

    public Object[] dataSocksServerPort() throws Exception {
        return new Object[] {1024};
    }

    public Object[] dataSocksVersion() throws Exception {
        return new Object[] {-1, 4, 5};
    }

    public Object[] dataStartingSequenceNumber() {
        return new Object[] {1L, 100L, 1000L};
    }
}
