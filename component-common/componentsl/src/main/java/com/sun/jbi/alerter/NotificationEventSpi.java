package com.sun.jbi.alerter;

/**
 * @author theyoz
 * @since 27/09/18
 */
public class NotificationEventSpi {
    public NotificationEvent create() {
        return new NotificationEventImpl();
    }

    public NotificationEvent create(
            final String componentType,
            final String componentProjectPathName,
            final String componentName,
            final long timeStamp) {
        return new NotificationEventImpl(componentType, componentProjectPathName, componentName, timeStamp);
    }

    public NotificationEvent create(
            final String physicalHostName,
            final String environmentName,
            final String logicalHostName,
            final String serverType,
            final String serverName,
            final String componentType,
            final String componentProjectPathName,
            final String componentName,
            final String type,
            int severity,
            int operationalState,
            final String messageCode,
            final String[] messageCodeArgs,
            final String messageDetails) {
        return new NotificationEventImpl(
                physicalHostName,
                environmentName,
                logicalHostName,
                serverType,
                serverName,
                componentType,
                componentProjectPathName,
                componentName,
                type,
                severity,
                operationalState,
                messageCode,
                messageCodeArgs,
                messageDetails);
    }
}
