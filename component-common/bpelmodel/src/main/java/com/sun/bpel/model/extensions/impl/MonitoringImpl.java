/*
 * BEGIN_HEADER - DO NOT EDIT
 * 
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */
package com.sun.bpel.model.extensions.impl;

import javax.xml.namespace.QName;

import com.sun.bpel.model.From;
import com.sun.bpel.model.extensions.Monitoring;
import com.sun.bpel.model.impl.BPELElementImpl;
import com.sun.bpel.model.visitor.BPELVisitor;
import com.sun.bpel.model.wsdlmodel.impl.XMLAttributeImpl;
import com.sun.bpel.xml.NamespaceUtility;
import com.sun.bpel.xml.common.model.XMLAttribute;
import com.sun.bpel.xml.common.model.XMLDocument;
import com.sun.bpel.xml.common.model.XMLElement;
import com.sun.bpel.xml.common.visitor.Visitor;

/*
 * @(#)MonitoringImpl.java 
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 * Copyright 2019-2020 Pymma All Rights Reserved.
 * 
 * END_HEADER - DO NOT EDIT
 */
public class MonitoringImpl extends BPELElementImpl implements Monitoring  {

    /* */
    private static final long serialVersionUID = 1629181853423601199L;

    /**
     * QName object for SeeBeyond Private extension line label
     */
    public static final QName LOG_QNAME = NamespaceUtility.getQName(
            XMLElement.SBYNBPEL_RUNTIME_EXTN_NAMESPACE, Monitoring.TAG, XMLElement.SBYNBPEL_RUNTIME_EXTN_PREFIX);

    /* */
    private From mFrom;

    /**
     * Creates a new instance of LogImpl
     */
    public MonitoringImpl() {
        super();
        initMonitoring();
    }

    /**
     * Creates a new instance of LogImpl.
     *
     * @param d Owner document.
     */
    public MonitoringImpl(XMLDocument d) {
        super(d);
        initMonitoring();
    }

    /**
     * Initializes this class.
     */
    private void initMonitoring() {
        setLocalName(Monitoring.TAG);
        setQualifiedName(LOG_QNAME);
        xmlAttrs = new XMLAttribute[]{
            new XMLAttributeImpl(ATTR.LEVEL, String.class, false, null),
            new XMLAttributeImpl(ATTR.LOCATION, String.class, true, null)
        };
    }

    @Override
    public boolean accept(Visitor w) {
        BPELVisitor v = morphVisitor(w);
        if (traverseParentFirst(v)) {
            if (!v.visit(this)) {
                return false;
            }
        }

        if (!super.accept(v)) {
            return false;
        }

        if (traverseParentLast(v)) {
            if (!v.visit(this)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public From getFrom() {
        return mFrom;
    }

    @Override
    public String getLevel() {
        String level = xmlAttrs[LEVEL].getValue();
        if (level == null) {
            return DEFAULT_LEVEL;
        }
        return level;
    }

    @Override
    public String getLocation() {
        String location = xmlAttrs[LOCATION].getValue();
        if (location == null) {
            return DEFAULT_LOCATION;
        }
        return location;
    }

    @Override
    public void setFrom(From from) {
        super.addChild(from);
        mFrom = from;
    }

    @Override
    public void setLevel(String level) {
        setAttribute(LEVEL, level);
    }

    @Override
    public void setLocation(String location) {
        setAttribute(LOCATION, location);
    }

}
