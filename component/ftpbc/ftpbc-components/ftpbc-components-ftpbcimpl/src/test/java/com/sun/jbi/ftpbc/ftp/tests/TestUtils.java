package com.sun.jbi.ftpbc.ftp.tests;

import com.sun.jbi.ftpbc.ftp.FtpFileConfigConstants;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * openesb.ftp.groupname.key.name
 * openesb.ftp.groupname.key.value
 *
 * @author Yoram Halberstam <yoram.halberstam@gmail.com>
 * @since 17/01/18
 */
public class TestUtils {
    private static final String STRICT_FTP_TEST = "openesb.ftp.strict";
    public static final Map<String, Properties> DEFAULT_FTP_PROPERTIES = new HashMap<>();

    static {
        for (Object o: System.getProperties().keySet()) {
            String key = (String) o;
            String[] keys = key.split("\\.");

            if (keys.length == 5 &&
                    "openesb".equals(keys[0]) &&
                    "ftp".equals(keys[1]) &&
                    "name".equals(keys[4])) {
                String value = System.getProperty(key);

                String valueKey = String.format("openesb.ftp.%s.%s.value", keys[2], keys[3]);
                String valueValue = System.getProperty(valueKey);

                if (valueValue  == null) {
                    throw new IllegalArgumentException(
                            String.format("The key [%s] doesn't have a corresponding value [%s]", key, valueKey));
                }

                Properties prop;

                if (DEFAULT_FTP_PROPERTIES.containsKey(keys[2])) {
                    prop = DEFAULT_FTP_PROPERTIES.get(keys[2]);
                } else {
                    prop = new Properties();
                    DEFAULT_FTP_PROPERTIES.put(keys[2], prop);
                }

                prop.setProperty(value, valueValue);
            }
        }
    }

    public static File createTempDirectory() throws IOException {
        final File res = File.createTempFile(TestUtils.class.getName(), ".tmp");

        if (res.exists() && !res.delete()) {
            throw new IOException(
                    String.format("Couldn't create folder %s, temporary file did not delete", res.getAbsolutePath()));
        }

        if (!res.mkdirs()) {
            throw new IOException(String.format("Couldn't create folder %s", res.getAbsolutePath()));
        }

        return res;
    }

    public static int findFreePort() throws IOException {
        try(ServerSocket socket = new ServerSocket(0)) {
            return socket.getLocalPort();
        }
    }

    public static String getTempFolder() {
        return System.getProperty("java.io.tmpdir");
    }

    public static boolean isPositiveInteger(String s) {
        try {
            return Integer.parseInt(s) >= 0;
        } catch (Throwable t) {
            return false;
        }
    }
    public static Properties ftpServerConfiguration(String config) {
        Properties res = DEFAULT_FTP_PROPERTIES.get(config);

        if (res == null && System.getProperty(STRICT_FTP_TEST) != null) {
            throw new IllegalArgumentException(
                    String.format(
                            "The ftp unit test requested config name [%s] which is not defined. The system is strict has -D%s has been defined. Either make it unstrict or configure the FTP configuration in your command line or settings.xml.",
                            config,
                            STRICT_FTP_TEST));
        }

        return res;
    }
}
