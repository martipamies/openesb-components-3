package com.sun.jbi.restbc.jbiadapter;

import com.sun.jbi.restbc.jbiadapter.descriptor.Filter;
import com.sun.jbi.restbc.jbiadapter.org.json.JSONObject;
import com.sun.jbi.restbc.jbiadapter.util.*;
import com.sun.jbi.restbc.jbiadapter.wsdl.RestOperation;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.api.uri.UriTemplate;
import com.sun.jersey.client.urlconnection.HTTPSProperties;
import com.sun.jersey.client.urlconnection.URLConnectionClientHandler;
import com.sun.wsdl4j.ext.impl.PartEx;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.jbi.messaging.NormalizedMessage;
import javax.mail.util.ByteArrayDataSource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import javax.wsdl.Definition;
import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;

/**
 * JerseyClientWrapper.java
 *
 * @author Edward Chou
 */
public final class JerseyClientWrapper {

    /*
     * 81-100
     */
    private static final Logger LOGGER = Logger.getLogger(JerseyClientWrapper.class.getName());
    private static JerseyClientWrapper instance = null;

    private static final MediaType MEDIA_TYPE_DEFAULT = MediaType.APPLICATION_JSON_TYPE;
    private static final Charset CHARSET_DEFAULT = Charset.forName("utf-8");
    private static final String[] ALLOWED_MEDIA_TYPES = {"application/pdf", "application/json", "text/json", "application/fhir+json", "application/fhir+xml", "application/xml", "text/plain", "text/xml", "application/x-www-form-urlencoded", "application/msword", "application/vnd.ms-excel", "application/vnd.ms-powerpoint", "image/bmp", "image/gif", "image/jpeg", "image/tiff"};
    private static final String ALLOWED_MEDIA_TYPES_STRING = "application/json,application/xml,text/plain,text/xml,application/fhir+json, application/fhir+xml, text/json,application/x-www-form-urlencoded, application/pdf, application/msword,application/vnd.ms-excel,application/vnd.ms-powerpoint,image/bmp,image/gif,image/jpeg,image/tiff";

    private JerseyClientWrapper() throws Exception {
    }

    public static synchronized JerseyClientWrapper getInstance() throws Exception {
        if (instance == null) {
            instance = new JerseyClientWrapper();
        }
        return instance;
    }

    public ClientRequest buildClientRequest(
            RestComponent component,
            NormalizedMessage requestMsg,
            OutboundConfiguration outboundConfig) throws Exception {

        // build URL
        String url = PropertiesUtil.safeGetProperty(requestMsg, NMProps.NM_URL_PROP);
        if (url.length() == 0) {
            url = outboundConfig.getUrl();
        }
        if (url.length() == 0) {
            throw new Exception("invalid HTTP request URL: " + url);
        }

        // build URL template from Path Parameters
        Map<String, String> pathParams = NMPropertiesUtil.getDynamicNMProperties(requestMsg, NMProps.NM_PATH_PARAMS_PROP);

        UriTemplate uriTemplate = new UriTemplate(url);
        String resultURL = uriTemplate.createURI(pathParams);

        UriBuilder uriBuilder = UriBuilder.fromUri(resultURL);

        // build params
        String paramStyle = PropertiesUtil.safeGetProperty(requestMsg, NMProps.NM_PARAM_STYLE_PROP);
        if (paramStyle.length() == 0) {
            paramStyle = outboundConfig.getParamStyle();
        }

        Map<String, String> params = NMPropertiesUtil.getDynamicNMProperties(requestMsg, NMProps.NM_PARAMS_PROP);

        // merge the parameters defined in the service unit
        for (Map.Entry<String, String> param : outboundConfig.getParams().entrySet()) {
            if (!params.containsKey(param.getKey())) {
                params.put(param.getKey(), param.getValue());
            }
        }
        // build the URI with params
        for (Map.Entry<String, String> param : params.entrySet()) {
            if (paramStyle.equalsIgnoreCase("MATRIX")) {
                uriBuilder.matrixParam(param.getKey(), param.getValue());
            } else {
                uriBuilder.queryParam(param.getKey(), param.getValue());
            }
        }

        // build method
        String requestMethod = PropertiesUtil.safeGetProperty(requestMsg, NMProps.NM_METHOD_PROP);
        if (requestMethod.length() == 0) {
            requestMethod = outboundConfig.getMethod();
        }

        /**
         * Set up the content type. Check the message property first If there is
         * no NM property for the content type, use the content type defined in
         * the WSDL. If there is no content type in the WSDL we setup the
         * content type with application/json;charset=utf-8
         */
        
        // let's find the content type 
        MediaType contentMediaType ; 
        
        /* 
        Priority is given to the message properties defined at the Mapper level
        If there is no parameters, we check if the outbound config is empty or not 
        In that case (empty) we set the content type with application/Json;charset=utf-8
        */
        
        // Check the message properties contan a content type 
        String contentTypeParameter = PropertiesUtil.safeGetProperty(requestMsg, NMProps.NM_CONTENT_TYPE_PROP);
        if ((contentTypeParameter == null) || (contentTypeParameter.length() == 0)) {
            // If there is no content type in the message properties 
            // We check if the content type is in the outbound configuration
            if ((outboundConfig.getContentType() == null) || (outboundConfig.getContentType().length() == 0)) {
                // if the content type in the Outboud configuration is null 
                // set with the default value
                Map <String,String> parameters = new HashMap<>(); 
                parameters.put ("charset",CHARSET_DEFAULT.name());
                contentMediaType = new MediaType (MEDIA_TYPE_DEFAULT.getType(),MEDIA_TYPE_DEFAULT.getSubtype(), parameters);
                contentTypeParameter = MEDIA_TYPE_DEFAULT.toString() + ";charset=" + CHARSET_DEFAULT.toString().toLowerCase();
            } else {                
                // Get the content type from the outbound config 
                contentTypeParameter = outboundConfig.getContentType();
            }
        } 

        /**
         * Second step: check if the content type is application/json
         * application/xml or text/plain or url encoded. The component supports
         * other binaries mime type. The component aims to limit the number of
         * type for example html type since the component is not plan to support
         * all the possible types. If the type does not is not supported we
         * throw an exception. In the same way, the content type charset "MUST"
         * be UTF-8 since the component is not designed to support other
         * encoding we Jersey by If the charset is not UTF-8 an exception is
         * sent In the outbound configuration, the content type is a String and
         * not a media type has found in the inbound configuration. So we play
         * with String and not media type
         *
         */
        String contentTypePart01 = "", contentTypePart02 = "";
        // Check if the content-Type contains the word charset         
        if (contentTypeParameter.contains ("charset")) {
            int index = contentTypeParameter.indexOf(";");
            contentTypePart01 = contentTypeParameter.substring(0, index).trim();
            index = contentTypeParameter.indexOf("charset");
            contentTypePart02 = contentTypeParameter.substring(index,contentTypeParameter.length());
            index = contentTypePart02.indexOf("=");
            contentTypePart02 = contentTypePart02.substring(index + 1).trim();            
        } else {
            contentTypePart01 = contentTypeParameter;
            contentTypePart02 = CHARSET_DEFAULT.name();
        }
        // the variable contentTypePart01 contains the content type String 
        // the variable charset contains the charset 

        // Throw exception if the Media type is not allowed    
        if (!(Arrays.asList(ALLOWED_MEDIA_TYPES).contains(contentTypePart01))) {
            StringBuilder msg = new StringBuilder();
            msg.append("The content-type ").append(contentTypePart01).append("is not supported by this version of the REST BC. The REST BC currently supported types are ");
            msg.append(ALLOWED_MEDIA_TYPES_STRING).append(".");
            LOGGER.severe(msg.toString());
            throw new Exception(msg.toString());
        }

        // Throw exception if the charset is not UTF-8
        if (!(CHARSET_DEFAULT.toString().equalsIgnoreCase(contentTypePart02.trim()))) {
            StringBuilder msg = new StringBuilder();
            msg.append("The charset of the content-type  ").append(contentTypePart01).append("is not supported by this version of the REST BC. Outbound content-type MUST be set with the charset").append(CHARSET_DEFAULT.name());
            LOGGER.severe(msg.toString());
            throw new Exception(msg.toString());
        }

        // build date
        String date = PropertiesUtil.safeGetProperty(requestMsg, NMProps.NM_DATE_PROP);

        // build acceptTypes
        String acceptTypesStr = PropertiesUtil.safeGetProperty(requestMsg, NMProps.NM_ACCEPT_TYPES_PROP);
        List<String> acceptTypes = null;
        if (acceptTypesStr.length() > 0) {
            acceptTypes = JsonUtil.parseJsonList(acceptTypesStr);
        }
        if (acceptTypes == null) {
            acceptTypes = new ArrayList<String>();
        }
        List<MediaType> acceptMediaTypes = new ArrayList<MediaType>();
        for (String s : acceptTypes) {
            acceptMediaTypes.add(MediaType.valueOf(s));
        }

        // merge the acceptTypes defined in the service unit
        for (MediaType acceptType : outboundConfig.getAcceptMediaTypes()) {
            if (!acceptMediaTypes.contains(acceptType)) {
                acceptMediaTypes.add(acceptType);
            }
        }

        // Check is the acceptType is null and set it as application/json per default 
        // then check is an accepted media type is different from the allowed mediatype
        if (acceptMediaTypes.isEmpty()) {
            acceptMediaTypes.add(MEDIA_TYPE_DEFAULT);
        } else {
            // check is the media type belong to the allowed mediatype
            for (MediaType mediaType : acceptMediaTypes) {
                String mediaTypeString = mediaType.getType() + "/" + mediaType.getSubtype();
                if (!(Arrays.asList(ALLOWED_MEDIA_TYPES).contains(mediaTypeString))) {
                    StringBuilder msg = new StringBuilder();
                    msg.append("The accepted media type type ").append(mediaType).append(" is not supported by this version of the REST BC. The REST BC currently supported types are ");
                    msg.append(ALLOWED_MEDIA_TYPES_STRING).append(".");
                    LOGGER.severe(msg.toString());
                    throw new Exception(msg.toString());
                }
                if (mediaType.getParameters().containsKey("charset")) {
                    String charset = mediaType.getParameters().get("charset");
                    if (!charset.isEmpty()) {
                        if (!charset.equalsIgnoreCase(CHARSET_DEFAULT.name())) {
                            StringBuilder msg = new StringBuilder();
                            msg.append("The charset ").append(charset).append(" is not supported by this version of the REST BC. Content-type MUST be set with the charset").append(CHARSET_DEFAULT.name());
                            LOGGER.severe(msg.toString());
                            throw new Exception(msg.toString());
                        }
                    }
                }
            }
        }

        // build acceptLanguages
        String acceptLanguagesStr = PropertiesUtil.safeGetProperty(requestMsg, NMProps.NM_ACCEPT_LANGUAGES_PROP);
        List<String> acceptLanguages = null;
        if (acceptLanguagesStr.length() > 0) {
            acceptLanguages = JsonUtil.parseJsonList(acceptLanguagesStr);
        }
        if (acceptLanguages == null) {
            acceptLanguages = new ArrayList<String>();
        }
        // merge the acceptLanguages defined in the service unit
        for (String s : outboundConfig.getAcceptLanguages()) {
            if (!acceptLanguages.contains(s)) {
                acceptLanguages.add(s);
            }
        }

        Map<String, String> headers = NMPropertiesUtil.getDynamicNMProperties(requestMsg, NMProps.NM_HEADERS_PROP);
        // merge the headers defined in the service unit
        for (Map.Entry<String, String> header : outboundConfig.getHeaders().entrySet()) {
            if (!headers.containsKey(header.getKey())) {
                headers.put(header.getKey(), header.getValue());
            }
        }

        // create ClientRequest.Builder
        ClientRequest.Builder requestBuilder = ClientRequest.create();

        boolean isContentTypeSet = false;
        if (contentTypeParameter.length() > 0) {
            requestBuilder.type(contentTypeParameter);
            isContentTypeSet = true;
        }

        boolean isDateSet = false;
        if (date.length() > 0) {
            requestBuilder.header("Date", date);
            isDateSet = true;
        }

        boolean isAcceptSet = false;
        for (MediaType acceptType : acceptMediaTypes) {
            requestBuilder.accept(acceptType);
            isAcceptSet = true;
        }

        /**
         * Accept-Charset forced to utf-8
         */
        requestBuilder.header("Accept-Charset", CHARSET_DEFAULT.name());

        boolean isAcceptLanguageSet = false;
        for (String acceptLanguage : acceptLanguages) {
            requestBuilder.acceptLanguage(acceptLanguage);
            isAcceptLanguageSet = true;
        }

        for (Map.Entry<String, String> header : headers.entrySet()) {
            // filter out headers that may conflict with the well-known headers set previously already
            if (header.getKey().equalsIgnoreCase("Content-Type") && isContentTypeSet) { // NOI18N
                String msg = I18n.loc("RESTBC-6081: Skipping Content-Type specified in Headers NM property, since Content-Type is already specified.");//NOI18N
                LOGGER.warning(msg);
                continue;
            }
            if (header.getKey().equalsIgnoreCase("Accept") && isAcceptSet) { // NOI18N
                String msg = I18n.loc("RESTBC-6082: Skipping Accept specified in Headers NM property, since Accept is already specified.");//NOI18N
                LOGGER.warning(msg);
                continue;
            }
            if (header.getKey().equalsIgnoreCase("Accept-Language") && isAcceptLanguageSet) { // NOI18N
                String msg = I18n.loc("RESTBC-6083: Skipping Accept-Language specified in Headers NM property, since Accept-Language is already specified.");//NOI18N
                LOGGER.warning(msg);
                continue;
            }

            if (header.getKey().equalsIgnoreCase("Date") && isDateSet) { // NOI18N
                String msg = I18n.loc("RESTBC-6084: Skipping Date specified in Headers NM property, since Date is already specified.");//NOI18N
                LOGGER.warning(msg);
                continue;
            }

            requestBuilder.header(header.getKey(), header.getValue());
        }

        URI uri = uriBuilder.build();
        ClientRequest clientRequest = requestBuilder.build(uri, requestMethod);

        // set security if necessary
        if (uri.getScheme() != null && uri.getScheme().equalsIgnoreCase("https")) { // NOI18N
            SSLContext sslContext = component.getSslContext();  // get SSLContext if truststore and keystore are available
            if (sslContext != null) {
                HostnameVerifier hostnameVerifier = null;
                if (component.getRuntimeConfig().isEnableHostnameVerifier() == false) {
                    hostnameVerifier = new HostnameVerifier() {
                        public boolean verify(String hostname, SSLSession session) {
                            return true;
                        }
                    };
                }

                HTTPSProperties prop = new HTTPSProperties(hostnameVerifier, sslContext);
                clientRequest.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, prop);

            } else {
                String msg = I18n.loc("RESTBC-7081: SSLContext is unavailable to make HTTPS Request");//NOI18N
                LOGGER.severe(msg);
                throw new Exception(msg);
            }

        }

        // set entity
        if (requestMethod.equalsIgnoreCase("GET") || requestMethod.equalsIgnoreCase("DELETE") || requestMethod.equalsIgnoreCase("HEAD")) { // NOI18N
            if (LOGGER.isLoggable(Level.FINEST)) {
                String msg = I18n.lf("RESTBC-1081: ignored reading payload for {0} method", requestMethod);//NOI18N
                LOGGER.finest(msg);
            }
            return clientRequest;
        }

        // use the entity as payload
        Object entity = requestMsg.getProperty(NMProps.NM_ENTITY_PROPS);
        if (entity != null) {
            if (entity instanceof String) {
                if (LOGGER.isLoggable(Level.FINEST)) {
                    String msg = I18n.lf("RESTBC-1082: sending entity located in NM property for the request as a String object");//NOI18N
                    LOGGER.finest(msg);
                }

            }
            byte[] bytes = ((String) entity).getBytes(CHARSET_DEFAULT);
            String entityString = new String(bytes);
            //String entityString = (String) entity;
            clientRequest.setEntity(entityString);
            return clientRequest;
        } else if (entity instanceof org.w3c.dom.Node) {
            if (LOGGER.isLoggable(Level.FINEST)) {
                String msg = I18n.lf("RESTBC-1083: sending entity located in NM property for the request as a DOMSource object");//NOI18N
                LOGGER.finest(msg);
            }
            org.w3c.dom.Node entityNode = (org.w3c.dom.Node) entity;
            DOMSource domSource = new DOMSource(entityNode);
            if (isContentTypeSet && PathUtil.isJSONMediaType(contentTypeParameter)) {
                // here convertXmlToString
                String xmlPayloadAsString = JbiMessageUtil.convertXmlToString(domSource);
                com.sun.jbi.restbc.jbiadapter.org.json.JSONObject jsonObject = com.sun.jbi.restbc.jbiadapter.org.json.XML.toJSONObject(xmlPayloadAsString);
                /**
                 * ESBCOM-161
                 */
                // Remove the XMLNameSpace if exists
                jsonObject = this.removeXSLNSFromJSONObject(jsonObject);

                // Remove the root is required in the outboundconfiguration
                if (outboundConfig.isRootRemoved()) {
                    jsonObject = removeRootFromJSONObject(jsonObject);
                }
                //ESBCOM-161 End 

                if (jsonObject != null) {
                    clientRequest.setEntity(jsonObject.toString());
                } else {
                    clientRequest.setEntity(xmlPayloadAsString);
                }
            } else if (PathUtil.isXMLMediaType(contentTypeParameter)) {
                clientRequest.setEntity(JbiMessageUtil.convertXmlToString(domSource));
            } else if (PathUtil.isTextPlainMediaType(contentTypeParameter)) {
                clientRequest.setEntity(JbiMessageUtil.convertXmlToString(domSource, true));
            } else {
                clientRequest.setEntity(JbiMessageUtil.convertXmlToString(domSource));
            }
            return clientRequest;
        } else if (entity instanceof Source) {
            if (LOGGER.isLoggable(Level.FINEST)) {
                String msg = I18n.lf("RESTBC-1084: sending entity located in NM property for the request as a Source object");//NOI18N
                LOGGER.finest(msg);
            }
            Source entitySource = (Source) entity;
            if (isContentTypeSet && PathUtil.isJSONMediaType(contentTypeParameter)) {
                String xmlPayloadAsString = JbiMessageUtil.convertXmlToString(entitySource);
                com.sun.jbi.restbc.jbiadapter.org.json.JSONObject jsonObject = com.sun.jbi.restbc.jbiadapter.org.json.XML.toJSONObject(xmlPayloadAsString);
                /**
                 * ESBCOM-161
                 */
                // Remove the XMLNameSpace if exists
                jsonObject = this.removeXSLNSFromJSONObject(jsonObject);

                // Remove the root is required in the outboundconfiguration
                if (outboundConfig.isRootRemoved()) {
                    jsonObject = removeRootFromJSONObject(jsonObject);
                }
                //ESBCOM-161 End 

                if (jsonObject != null) {
                    clientRequest.setEntity(jsonObject.toString());
                } else {
                    clientRequest.setEntity(xmlPayloadAsString);
                }
            } else {
                clientRequest.setEntity(JbiMessageUtil.convertXmlToString(entitySource));
            }
            return clientRequest;
        }

        // use JBI payload
        Object requestPayload = JbiMessageUtil.getPayloadFromWrappedMsg(requestMsg);
        if (requestPayload != null) {
            if (requestPayload instanceof Source) {
                Source xmlPayload = (Source) requestPayload;
                if (isContentTypeSet && PathUtil.isJSONMediaType(contentTypeParameter)) {
                    String xmlPayloadAsString = JbiMessageUtil.convertXmlToString(xmlPayload);
                    com.sun.jbi.restbc.jbiadapter.org.json.JSONObject jsonObject = com.sun.jbi.restbc.jbiadapter.org.json.XML.toJSONObject(xmlPayloadAsString);
                    /**
                     * ESBCOM-161
                     */
                    // Remove the XMLNameSpace if exists
                    jsonObject = this.removeXSLNSFromJSONObject(jsonObject);

                    // Remove the root is required in the outboundconfiguration
                    if (outboundConfig.isRootRemoved()) {
                        jsonObject = removeRootFromJSONObject(jsonObject);
                    }
                    //ESBCOM-161 End 
                    if (jsonObject != null) {
                        clientRequest.setEntity(jsonObject.toString());
                    } else {
                        clientRequest.setEntity(xmlPayloadAsString);
                    }
                } else if (PathUtil.isXMLMediaType(contentTypeParameter)) {
                    clientRequest.setEntity(JbiMessageUtil.convertXmlToString(xmlPayload));
                } else if (PathUtil.isTextPlainMediaType(contentTypeParameter)) {
                    clientRequest.setEntity(JbiMessageUtil.convertXmlToString(xmlPayload, true));
                } else if (PathUtil.isUrlEncodedMediaType(contentTypeParameter)) {
                    clientRequest.setEntity(UrlQueryHelper.QueryFromXml(JbiMessageUtil.convertXmlToString(xmlPayload, true)));
                } else {
                    DataHandler streamPayload = (DataHandler) requestPayload;
                    clientRequest.setEntity(streamPayload.getInputStream());
                }
            }
        }
        return clientRequest;
    }

    public void buildNormalizedReplyMessage(NormalizedMessage replyMsg,
            ClientRequest clientRequest,
            ClientResponse clientResponse,
            OutboundConfiguration outboundConfig) throws Exception {

        String method = clientRequest.getMethod();

        replyMsg.setProperty(NMProps.NM_RESPONSE_STATUS_PROP, clientResponse.getStatus());

        if (clientResponse.getLocation() != null) {
            replyMsg.setProperty(NMProps.NM_RESPONSE_URL_PROP, clientResponse.getLocation().toString());
        }

        if (clientResponse.getType() != null) {
            replyMsg.setProperty(NMProps.NM_RESPONSE_CONTENT_TYPE_PROP, MediaTypeUtil.mediaTypeToString(clientResponse.getType()));
        }

        /**
         * Here we check if the response content type is compliant with the REST
         * BC i.e: application/json, application/xml, text/plain
         */
        String responseMediaTypeString = clientResponse.getType().getType() + "/" + clientResponse.getType().getSubtype();
        if (!(Arrays.asList(ALLOWED_MEDIA_TYPES).contains(responseMediaTypeString))) {
            StringBuilder msg = new StringBuilder();
            msg.append("The content-type ").append(clientResponse.getType().toString()).append(" provided in the response is not supported by this version of the REST BC. The REST BC currently supported types are ");
            msg.append(ALLOWED_MEDIA_TYPES_STRING).append(".");
            LOGGER.severe(msg.toString());
            throw new Exception(msg.toString());
        }

        /**
         * Check if there is a charset associated with the content-type if empty
         * then UTF-8, else we check if this charset is UTF-8. If not exception
         * .
         */
        String responseCharsetString = clientResponse.getType().getParameters().get("charset");
        if (responseCharsetString == null) {
            responseCharsetString = CHARSET_DEFAULT.name();
        } else {
            // Check that the response is UTF-8
            if (!responseCharsetString.equalsIgnoreCase(CHARSET_DEFAULT.name())) {
                StringBuilder msg = new StringBuilder();
                msg.append("The content charset ").append(responseCharsetString).append(" provided in the response is not supported. The REST BC support the charset UFT-8 only.");
                LOGGER.severe(msg.toString());
                throw new Exception(msg.toString());
            }
        }

        Map<String, String> headers = NMPropertiesUtil.multivaluedMapToMap(clientResponse.getHeaders());
        replyMsg.setProperty(NMProps.NM_RESPONSE_HEADERS_PROP, JsonUtil.buildJson(headers));
        NMPropertiesUtil.setDynamicNMProperties(replyMsg, NMProps.NM_RESPONSE_HEADERS_PROP, headers);

        InputStream inputStream = clientResponse.getEntity(InputStream.class);

        RestOperation restOp = outboundConfig.getRestOp();
        Definition definition = outboundConfig.getDefinition();
        if (restOp != null && definition != null) {
            // has WSDL configuration

            Source replyContent = null;
            if (inputStream != null && clientResponse.getType() != null && !method.equalsIgnoreCase("head")) {
                MediaType mediaType = clientResponse.getType();
                if (PathUtil.isXMLMediaType(mediaType) && !outboundConfig.isForwardAsAttachment()) {
                    // this is XML content and don't forward as attachment

                    replyContent = JbiMessageUtil.createJbiWrappedMsg(inputStream, restOp, definition, true);
                    //replyContent = JbiMessageUtil.createJbiWrappedMsg(inputStream, restOp, definition, true);
                } else if (PathUtil.isJSONMediaType(mediaType) && !outboundConfig.isForwardAsAttachment()) {
                    // this is JSON content, convert to XML and don't forward as attachment
                    StringBuilder sb = new StringBuilder();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, CHARSET_DEFAULT));
                    String currentString = reader.readLine();
                    while (currentString != null) {
                        sb.append(currentString);
                        currentString = reader.readLine();
                    }
                    com.sun.jbi.restbc.jbiadapter.org.json.JSONObject jsonObject = new com.sun.jbi.restbc.jbiadapter.org.json.JSONObject(sb.toString());
                    String xmlString = com.sun.jbi.restbc.jbiadapter.org.json.XML.toString(jsonObject);
                    /**
                     * author: Paul Perez Jira issue: ESBCOMP-160 Unable to use
                     * JSON in the Body Request Add nameSpace to the xmlString
                     */
                    if (outboundConfig.isRootRequired()) {
                        xmlString = this.addNameSpaceToRawXML(restOp, xmlString, outboundConfig.getRootName());
                    } else {
                        xmlString = this.addNameSpaceToRawXML(restOp, xmlString);
                    }
                    // End of Modification  ESB-COMP-160
                    replyContent = JbiMessageUtil.createJbiWrappedMsg(xmlString, restOp, definition, true);

                    /**
                     * ESBCOMP-161 Support text/plain
                     */
                } else if (PathUtil.isTextPlainMediaType(mediaType) && !outboundConfig.isForwardAsAttachment()) {
                    replyContent = JbiMessageUtil.createJbiWrappedMsg(inputStream, restOp, definition, true);
                    //replyContent = JbiMessageUtil.createJbiWrappedMsg(inputStream, restOp, definition, true);
                } else {
                    // treat as attachment
                    DataSource ds = new ByteArrayDataSource(inputStream, MediaTypeUtil.mediaTypeToString(mediaType));
                    DataHandler dataHandler = new DataHandler(ds);
                    String uuid = UUID.randomUUID().toString();

                    replyContent = JbiMessageUtil.createJbiAttachmentWrapper(uuid, restOp, definition, true);
                    replyMsg.addAttachment(uuid, dataHandler);
                }
            } else {
                replyContent = JbiMessageUtil.createEmptyJbiWrappedMsg(restOp, definition, true);
            }
            replyMsg.setContent(replyContent);
        } else {
            // no WSDL configuration
            Source replyContent = null;
            QName msgType = null;
            try {
                msgType = QName.valueOf(outboundConfig.getMsgType());
            } catch (IllegalArgumentException iae) {
                // ignore
            }
            if (inputStream != null && clientResponse.getType() != null && !method.equalsIgnoreCase("head")) {
                MediaType mediaType = clientResponse.getType();
                if (PathUtil.isXMLMediaType(mediaType) && !outboundConfig.isForwardAsAttachment()) {
                    // this is XML content and don't forward as attachment
                    /**
                     * Take charset into account
                     */
                    replyContent = JbiMessageUtil.createJbiWrappedMsg(msgType, inputStream);
                } else if (PathUtil.isJSONMediaType(mediaType) && !outboundConfig.isForwardAsAttachment()) {
                    // this is JSON content, convert to XML and don't forward as attachment
                    StringBuilder sb = new StringBuilder();
                    /**
                     * Take charset into account
                     */
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, CHARSET_DEFAULT));
                    String currentString = reader.readLine();
                    while (currentString != null) {
                        sb.append(currentString);
                        currentString = reader.readLine();
                    }
                    com.sun.jbi.restbc.jbiadapter.org.json.JSONObject jsonObject = new com.sun.jbi.restbc.jbiadapter.org.json.JSONObject(sb.toString());
                    String xmlString = com.sun.jbi.restbc.jbiadapter.org.json.XML.toString(jsonObject);
                    replyContent = JbiMessageUtil.createJbiWrappedMsg(msgType, new ByteArrayInputStream(xmlString.getBytes()));

                    /**
                     * ESBCOMP-161 Add TextPlain support
                     */
                } else if (PathUtil.isTextPlainMediaType(mediaType) && !outboundConfig.isForwardAsAttachment()) {
                    replyContent = JbiMessageUtil.createJbiWrappedMsg(inputStream, restOp, definition, true);
                } else {
                    // treat as attachment
                    DataSource ds = new ByteArrayDataSource(inputStream, MediaTypeUtil.mediaTypeToString(clientResponse.getType()));
                    DataHandler dataHandler = new DataHandler(ds);
                    String uuid = UUID.randomUUID().toString();

                    replyContent = JbiMessageUtil.createJbiAttachmentWrapper(msgType, uuid);
                    replyMsg.addAttachment(uuid, dataHandler);
                }

            } else {
                replyContent = JbiMessageUtil.createJbiWrappedMsg(msgType, null);
            }
            replyMsg.setContent(replyContent);
        }

    }

    public ClientResponse makeRequest(ClientRequest request,
            NormalizedMessage requestMsg,
            OutboundConfiguration outboundConfig) throws Exception {

        Client client = new Client(new URLConnectionClientHandler(new HttpProxyURLConnectionFactory()));

        // add basic auth filter
        String basicAuthUsername = PropertiesUtil.safeGetProperty(requestMsg, NMProps.NM_BASICAUTH_USERNAME_PROP);
        if (basicAuthUsername.length() == 0) {
            basicAuthUsername = outboundConfig.getBasicAuthUsername();
        }

        String basicAuthPassword = PropertiesUtil.safeGetProperty(requestMsg, NMProps.NM_BASICAUTH_PASSWORD_PROP);
        if (basicAuthPassword.length() == 0) {
            basicAuthPassword = outboundConfig.getBasicAuthPassword();
        }

        if (basicAuthUsername.length() > 0 && basicAuthPassword.length() > 0) {
            client.addFilter(new HTTPBasicAuthFilter(basicAuthUsername, basicAuthPassword));
        }

        if (LOGGER.isLoggable(Level.FINEST)) {
            LOGGER.finest(getClientRequestAsString(request));
        }

        // apply all custom filters
        for (Filter filter : outboundConfig.getEndpointIdentifier().getFilters()) {
            try {
                Class<?> filterClass = outboundConfig.getServiceUnit().loadFilterClass(filter.getClassName());

                if (!com.sun.jersey.api.client.filter.ClientFilter.class
                        .isAssignableFrom(filterClass)) {
                    String msg = I18n.lf("RESTBC-6085: filter class {0} is not a subclass of com.sun.jersey.api.client.filter.ClientFilter, skipping ...", filter.getClassName());//NOI18N
                    LOGGER.warning(msg);
                    continue;
                }
                Object filterObject = filterClass.newInstance();

                for (Map.Entry<String, String> entry : filter.getProps().entrySet()) {
                    String key = entry.getKey();
                    String val = entry.getValue();
                    BeanUtil.setProperty(filterObject, key, val);
                }

                client.addFilter((com.sun.jersey.api.client.filter.ClientFilter) filterObject);

            } catch (Exception e) {
                String msg = I18n.lf("RESTBC-6086: unable to instantiate filter class {0} skipping ..., {1}", filter.getClassName(), e);//NOI18N
                LOGGER.warning(msg);
                continue;
            }
        }

        ClientResponse response = client.handle(request);

        if (LOGGER.isLoggable(Level.FINEST)) {
            LOGGER.finest(getClientResponseAsString(response));
        }

        return response;
    }

    private String getClientRequestAsString(ClientRequest request) {
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("Client Request: \n");
            sb.append("  URI = " + request.getURI() + "\n");
            sb.append("  Method = " + request.getMethod() + "\n");
            sb.append("  Headers = " + request.getMetadata() + "\n");
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "error logging client request", e);
        }
        return sb.toString();
    }

    private String getClientResponseAsString(ClientResponse response) {
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("Client Response: \n");
            sb.append("  Status = " + response.getResponseStatus() + " \n");
            sb.append("  Type = " + MediaTypeUtil.mediaTypeToString(response.getType()) + " \n");
            sb.append("  Headers = " + response.getHeaders() + " \n");
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "error logging client response", e);
        }
        return sb.toString();
    }

    /**
     * @ author: Paul Perez
     * @ Jira issue: ESBCOMP-160 Unable to use JSON in the Body Request
     * @ date: 12/07/2018
     * @param restOp
     * @return
     * @throws Exception
     *
     * Extract the namespace of the Schema use fir the input message If there
     * are multi part in the output message the Case is not supported
     */
    private String getOutputMessageNamespace(RestOperation restOp) throws Exception {
        Map<String, PartEx> parts = restOp.getOperation().getOutput().getMessage().getParts();
        if (parts.size() == 1) {
            Map.Entry entry = (Map.Entry) parts.entrySet().iterator().next();
            PartEx part = (PartEx) entry.getValue();
            String messageNameSpace = part.getElementName().getNamespaceURI();
            return messageNameSpace;
        } else {
            String msg = I18n.loc("RESTBC-7077: The input message must have one and only on part. Null or multi parts messages are not supported by the REST BC. The current input message defines {0} parts", parts.size());//NOI18N
            LOGGER.severe(msg);
            throw new Exception(msg);
        }
    }

    /**
     * @ author: Paul Perez
     * @ Jira issue: ESBCOMP-161 Unable to use JSON in the Body Request
     * @param xmlString
     * @param restOp
     * @return
     *
     * Add the nameSpace to the Raw XML generated by the code plus a Root often
     * not found in JSON
     *
     */
    private String addNameSpaceToRawXML(RestOperation restOp, String xmlString, String rootName) throws Exception {
        String nameSpace = "<" + rootName + " xmlns='" + this.getOutputMessageNamespace(restOp) + "'> ";
        xmlString = nameSpace + xmlString + "</" + rootName + ">";
        return xmlString;
    }

    /**
     * @ author: Paul Perez
     * @ Jira issue: ESBCOMP-161 Unable to use JSON in the Body Request
     * @param xmlString
     * @param restOp
     * @return
     *
     * Add the nameSpace to the Raw XML generated by the code
     *
     */
    private String addNameSpaceToRawXML(RestOperation restOp, String xmlString) throws Exception {
        String nameSpace = " xmlns='" + this.getOutputMessageNamespace(restOp) + "' ";
        // Since the XML is very raw, the elements don't contains nameSpace as attribut. So we just add xmlns='http...' just before the first ">"
        int indexOf = xmlString.indexOf(">");
        xmlString = new StringBuilder(xmlString).insert(indexOf, nameSpace).toString();
        return xmlString;
    }

    private JSONObject removeXSLNSFromJSONObject(JSONObject jsonObject) {
        /**
         * Here we suppose that the XML has one XML namespace for all the
         * document Here we just search for one XML Namespace and not two or
         * more
         */
        String firstKey = jsonObject.keys().next();
        JSONObject subJSonObject = jsonObject.getJSONObject(firstKey);
        // remove xmlns key if exists        
        subJSonObject.remove("xmlns");
        return jsonObject;
    }

    private JSONObject removeRootFromJSONObject(JSONObject jsonObject) {
        // search the Json document after the root 
        String firstKey = jsonObject.keys().next();
        JSONObject subJSonObject = jsonObject.getJSONObject(firstKey);
        return subJSonObject;
    }
}
