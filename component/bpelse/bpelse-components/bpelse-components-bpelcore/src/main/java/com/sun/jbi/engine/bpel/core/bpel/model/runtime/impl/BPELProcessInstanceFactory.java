package com.sun.jbi.engine.bpel.core.bpel.model.runtime.impl;

import com.sun.jbi.engine.bpel.core.bpel.engine.BPELProcessInstance;
import com.sun.jbi.engine.bpel.core.bpel.engine.BPELProcessManager;
import com.sun.jbi.engine.bpel.core.bpel.engine.Engine;

import java.util.Iterator;
import java.util.ServiceLoader;
import java.util.logging.Logger;

/**
 * @author theyoz
 * @since 26/09/18
 */
public class BPELProcessInstanceFactory {
    private static final Logger LOGGER = Logger.getLogger(BPELProcessInstanceFactory.class.getName());
    private static BPELProcessInstanceSpi factory;

    public static BPELProcessInstance newInstance(BPELProcessManager processManager, Engine eng, String bpInstanceId) {
        if (factory == null) {
            ServiceLoader<BPELProcessInstanceSpi> serviceLoader = ServiceLoader.load(BPELProcessInstanceSpi.class);

            Iterator<BPELProcessInstanceSpi> ite = serviceLoader.iterator();

            if (ite.hasNext()) {
                factory = ite.next();

                if (ite.hasNext()) {
                    LOGGER.warning(
                            String.format("Multiple BPELProcessInstanceSpi found, using %s", factory.getClass().getName()));
                }
            } else {
                factory = new BPELProcessInstanceSpi();
            }
        }

        return factory.create(processManager, eng, bpInstanceId);
    }
}
