/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-jbi-components.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FtpDirRegExpTest.java
 *
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ftpbc.ftp;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Properties;

/**
 * TestNG based test.
 *
 * @author Harry Liu (harry.liu@sun.com)
 * @author theyoz
 *
 * Copyright 2006 Sun Microsystems, Inc. All Rights Reserved.
 */
public class FtpDirRegExpTest extends AbstractFtpTest {
    @Override
    protected void beforeInitializeFtp(Properties props) {
        super.beforeInitializeFtp(props);
        props.put(FtpFileConfigConstants.C_P_GEN_CONN_MODE, "Automatic");
    }

    @Override
    protected void afterInitializeFtp() throws Exception {
        super.afterInitializeFtp();

        ftp.getProvider().setUseRegexAsMatcher(true);
    }

    /**
     * Test of getDirs method, of class com.sun.jbi.ftpbc.ftp.FtpDirRegExp.
     */
    @Test
    public void testGetDirs_0() throws Exception {
        ArrayList expResult = new ArrayList();
        FtpDirRegExp instance = new FtpDirRegExp("/not/existed/dir/pattern", ftp.getProvider());
        ArrayList result = instance.getDirs();
        assert Objects.equals(expResult, result) : "An empty ArrayList is returned for nothing-match";
    }

    /**
     * Test of getDirs method, of class com.sun.jbi.ftpbc.ftp.FtpDirRegExp.
     */
    @Test
    public void testGetDirs_1() throws Exception {
        FtpDirRegExp instance = new FtpDirRegExp("/Products/Windows", ftp.getProvider());
        ArrayList result = instance.getDirs();
        assert result.size() == 1;
        assert result.get(0).toString().replace('\\', '/').endsWith("/Products/Windows");
    }

    /**
     * Test of constructor, of class com.sun.jbi.ftpbc.ftp.FtpDirRegExp.
     */
    @Test
    public void testFtpDirRegExp() {
        new FtpDirRegExp("A pattern", new FtpFileProviderImpl());
        new FtpDirRegExp(null, new FtpFileProviderImpl());
        new FtpDirRegExp("A pattern", null);
        new FtpDirRegExp(null, null);
    }
}
