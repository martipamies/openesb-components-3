package com.sun.jbi.engine.bpel.core.bpel.xpath.functions.cache;

import com.sun.jbi.engine.bpel.core.bpel.cache.CacheManager;
import com.sun.jbi.engine.bpel.core.bpel.xpath.functions.BPWSFunctions;
import org.apache.commons.jxpath.ExpressionContext;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 */
public class RemoveCacheFunction extends AbstractCacheFunction {

    public RemoveCacheFunction(CacheManager cacheManager) {
        super(cacheManager);
    }
    
    public Object invoke(ExpressionContext context, Object[] parameters) {
        return cacheManager.remove((String) BPWSFunctions.convertParam(parameters[0]));
    }

}
